(function () {

  "use strict";

  var React = window.React
    , ReactCSSTransitionGroup = React.addons.CSSTransitionGroup
    , PRODUCTOS = null
    , MainHeader = null
    , Productos = null
    , Main = null
    , Application = null;

  PRODUCTOS = [
  ];

  /* MainHeader ***************************************************************/
  MainHeader = React.createClass({
    render: function () {
      return React.createElement(
        "header",
        { className: "mainHeader" },
        React.createElement(
          "a",
          { className: "logo", href: "/" },
          React.createElement(
            "img",
            { className: "logo-img", src: "../logo.png" }
          )
        )
      );
    }
  });

  /* Main *********************************************************************/
  Main = React.createClass({
    render: function () {
      return React.createElement(
        "section",
        { className: "mainContent" },
        React.createElement(
          "div",
          { className: "navList" },
          React.createElement(
            "a",
            { className: "navList-item button button--primary", onClick: this._buscarProducto },
            "Buscar Producto"
          ),
          React.createElement(
            "a",
            { className: "navList-item button button--primary", onClick: this._altaUsuario },
            "Alta de Usuario"
          ),
          React.createElement(
            "a",
            { className: "navList-item button button--primary", onClick: this._modificacionUsuario },
            "Modificación de Usuario"
          ),
          React.createElement(
            "a",
            { className: "navList-item button button--primary", onClick: this._bajaUsuario },
            "Baja de Usuario"
          )
        )
      );
    },
    _buscarProducto: function (event) {
      event.preventDefault();

      this.props.onViewChange("productos");
    },
    _altaUsuario: function (event) {
      event.preventDefault();

      this.props.onViewChange("alta-usuario");
    }
  });

  /* Productos ****************************************************************/
  Productos = React.createClass({
    render: function () {
      return React.createElement(
        "div",
        { className: "pageHeader" },
        "Productos",
        React.createElement(
          "a",
          { className: "pageHeader--secondaryItem", onClick: this._clickBack },
          "Atras"
        )
      );
    },
    _clickBack: function (event) {
      event.preventDefault();
      this.props.onBack();
    }
  });

  /* Application **************************************************************/
  Application = React.createClass({
    getInitialState: function () {
      return {
        view: "main"
      };
    },
    render: function () {
      var view = null;

      switch (this.state.view) {
      case "main":
        view = React.createElement(Main, { onViewChange: this._viewChange, key: "main" });
        break;
      case "productos":
        view = React.createElement(Productos, { onBack: this._back, key: "productos" });
        break;
      }

      return React.createElement(
        "div",
        { className: "mobile" },
        React.createElement(MainHeader),
        React.createElement(
          "div",
          { className: "mobile-view" },
          React.createElement(
            ReactCSSTransitionGroup,
            { transitionName: "anim_slideLeft", component: "div", transitionLeave: false },
            view
          )
        )
      );
    },
    _viewChange: function (view) {
      this.setState({ view: view });
    },
    _back: function () {
      this._viewChange("main");
    }
  });

  window.onload = function () {
    React.render(React.createElement(Application), document.body);
  };

})();
